import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'
import DocumentTitle from 'react-document-title'

import Nav from './components/Nav';
import Home from './components/Home';
import Admin from './components/Admin';
import Cashflow from './components/Cashflow';
import Customers from './components/Customers';
import Dogs from './components/Dogs';
import Payments from './components/Payments';
import Rates from './components/Rates';
import Receipts from './components/Receipts';
import Reports from './components/Reports';
import Sales from './components/Sales';

const BasicExample = () => (
  <DocumentTitle title='Web App'>
    <Router>
      <div>
        <Nav />

        <hr/>

        <Switch>
          <Route path="/customers" component={Customers}/>
          <Route path="/cashflow/receipts/:id" component={Receipts}/>
          <Route path="/cashflow/receipts" component={Receipts}/>
          <Route path="/cashflow" component={Cashflow}/>
          <Route path="/cashflow/payments:id" component={Payments}/>
          <Route path="/cashflow/payments" component={Payments}/>
          <Route path="/dogs" component={Dogs}/>
          <Route path="/dogs/sales/:id" component={Sales}/>
          <Route path="/dogs/sales" component={Sales}/>
          <Route path="/dogs/reports/:id" component={Reports}/>
          <Route path="/dogs/reports" component={Reports}/>
          <Route path="/admin/rates" component={Rates}/>
          <Route path="/admin" component={Admin}/>
          <Route exact path="/" component={Home}/>
        </Switch>
      </div>
    </Router>
  </DocumentTitle>
)
export default BasicExample