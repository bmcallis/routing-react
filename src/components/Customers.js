import React from 'react';
import DocumentTitle from 'react-document-title'

const Customers = () => (
  <DocumentTitle title='Customers'>
    <div>
      <h2>Customers</h2>
      <p>These are all of the customers</p>
    </div>
  </DocumentTitle>
)

export default Customers;
