import React from 'react';
import DocumentTitle from 'react-document-title'

const Home = () => (
  <DocumentTitle title='Home'>
    <div>
      <h2>Home</h2>
      <p>Welcome</p>
    </div>
  </DocumentTitle>
)

export default Home;
