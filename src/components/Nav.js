import React from 'react';
import { NavLink } from 'react-router-dom'

const Nav = () => {
  return (
    <ul>
      <li><NavLink exact to="/">Home</NavLink></li>
      <li><NavLink to="/customers">Customers</NavLink></li>
      <li>
        <NavLink to="/cashflow">Cashflow</NavLink>
        <ul>
          <li><NavLink to="/cashflow/receipts">Receipts</NavLink></li>
          <li><NavLink to="/cashflow/payments">Payments</NavLink></li>
        </ul>
      </li>
      <li>
        <NavLink to="/dogs">Dogs</NavLink>
        <ul>
          <li><NavLink to="/dogs/sales">Sales</NavLink></li>
          <li><NavLink to="/dogs/reports">Reports</NavLink></li>
        </ul>
      </li>
      <li>
        <NavLink to="/admin">Admin</NavLink>
        <ul>
          <li><NavLink to="/admin/rates">Rates</NavLink></li>
        </ul>
      </li>
    </ul>
  )
}

export default Nav;