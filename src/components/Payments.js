import React from 'react';
import DocumentTitle from 'react-document-title'

const Payments = props => {
  console.log(props);
  const id = props.match.params.id;
  return (
    <DocumentTitle title={ `Receipt ${id}` }>
      <div>
        <h2>Payment Details</h2>
        <p>Details for payment</p>
      </div>
    </DocumentTitle>
  );
}

export default Payments;
