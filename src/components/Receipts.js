import React from 'react';
import DocumentTitle from 'react-document-title'
import { withRouter } from 'react-router-dom'

const Receipts = (props) => {
  const id = props.match.params.id;
  return (
    <DocumentTitle title={ `Receipt ${id}` }>
      <div>
        <h2>Receipt Details</h2>
        <p>{`Details for receipt ${id}`}</p>
      </div>
    </DocumentTitle>
  );
}

export default withRouter(Receipts);
