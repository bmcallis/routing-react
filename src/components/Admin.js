import React from 'react';
import DocumentTitle from 'react-document-title'

const Admin = () => (
  // TODO this file should just reroute to commissionRates
  <DocumentTitle title='Admin'>
    <div>
      <h2>Admin</h2>
      <p>You should be sent straight to the commission rates page</p>
    </div>
  </DocumentTitle>
)

export default Admin;
