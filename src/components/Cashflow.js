import React from 'react';
import DocumentTitle from 'react-document-title'

const Cashflow = () => (
  <DocumentTitle title='Cashflow'>
    <div>
      <h2>Cashflow</h2>
      <p>Links to payments or recipts</p>
    </div>
  </DocumentTitle>
)

export default Cashflow;
